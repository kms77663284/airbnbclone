package com.dto;

public class SiteSetting {
	float s_ver;
	String s_api1;
	String s_api2;
	String s_api3;
	String s_api4;
	String s_api5;
	String s_api6;
	String s_facebook;
	String s_instagram;
	String s_googleStore;
	String s_itunesStore;
	String s_intro;
	String s_logo;
	int s_min;
	int s_max;
	String s_main;
	
	public float getS_ver() {
		return s_ver;
	}
	public void setS_ver(float s_ver) {
		this.s_ver = s_ver;
	}
	public String getS_api1() {
		return s_api1;
	}
	public void setS_api1(String s_api1) {
		this.s_api1 = s_api1;
	}
	public String getS_api2() {
		return s_api2;
	}
	public void setS_api2(String s_api2) {
		this.s_api2 = s_api2;
	}
	public String getS_api3() {
		return s_api3;
	}
	public void setS_api3(String s_api3) {
		this.s_api3 = s_api3;
	}
	public String getS_api4() {
		return s_api4;
	}
	public void setS_api4(String s_api4) {
		this.s_api4 = s_api4;
	}
	public String getS_api5() {
		return s_api5;
	}
	public void setS_api5(String s_api5) {
		this.s_api5 = s_api5;
	}
	public String getS_api6() {
		return s_api6;
	}
	public void setS_api6(String s_api6) {
		this.s_api6 = s_api6;
	}
	public String getS_facebook() {
		return s_facebook;
	}
	public void setS_facebook(String s_facebook) {
		this.s_facebook = s_facebook;
	}
	public String getS_instagram() {
		return s_instagram;
	}
	public void setS_instagram(String s_instagram) {
		this.s_instagram = s_instagram;
	}
	public String getS_googleStore() {
		return s_googleStore;
	}
	public void setS_googleStore(String s_googleStore) {
		this.s_googleStore = s_googleStore;
	}
	public String getS_itunesStore() {
		return s_itunesStore;
	}
	public void setS_itunesStore(String s_itunesStore) {
		this.s_itunesStore = s_itunesStore;
	}
	public String getS_intro() {
		return s_intro;
	}
	public void setS_intro(String s_intro) {
		this.s_intro = s_intro;
	}
	public String getS_logo() {
		return s_logo;
	}
	public void setS_logo(String s_logo) {
		this.s_logo = s_logo;
	}
	public int getS_min() {
		return s_min;
	}
	public void setS_min(int s_min) {
		this.s_min = s_min;
	}
	public int getS_max() {
		return s_max;
	}
	public void setS_max(int s_max) {
		this.s_max = s_max;
	}
	public String getS_main() {
		return s_main;
	}
	public void setS_main(String s_main) {
		this.s_main = s_main;
	}
	
	
}
