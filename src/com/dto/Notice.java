package com.dto;

public class Notice {
	private int n_idx;
	private String a_id;
	private String n_title;
	private String n_content;
	private int n_vcount;
	private String n_date;
	public int getN_idx() {
		return n_idx;
	}
	
	public void setN_idx(int n_idx) {
		this.n_idx = n_idx;
	}
	public String getA_id() {
		return a_id;
	}
	public void setA_id(String a_id) {
		this.a_id = a_id;
	}
	public String getN_title() {
		return n_title;
	}
	public void setN_title(String n_title) {
		this.n_title = n_title;
	}
	public String getN_content() {
		return n_content;
	}
	public void setN_content(String n_content) {
		this.n_content = n_content;
	}
	public int getN_vcount() {
		return n_vcount;
	}
	public void setN_vcount(int n_vcount) {
		this.n_vcount = n_vcount;
	}
	public String getN_date() {
		return n_date;
	}
	public void setN_date(String n_date) {
		this.n_date = n_date;
	}

	
}
