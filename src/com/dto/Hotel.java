package com.dto;

public class Hotel {
	private int h_idx;
	private String h_id;
	private String h_introduce;
	private String h_other;
	private int h_price;
	private String h_title;
	private float h_star;
	private String h_address1;
	private String h_address2;
	private String h_address3;
	private String h_address4;
	private String h_zipcode;
	private int h_guests;
	private boolean h_grant;
	private String h_facilities;
	private String h_type;
	private String h_type2;
	private String h_rules;
	private String h_res_not_possible_date;
	private String h_potos;
	private String h_date;
	private double h_mapX;
	private double h_mapY;
	public int getH_idx() {
		return h_idx;
	}
	public void setH_idx(int h_idx) {
		this.h_idx = h_idx;
	}
	public String getH_id() {
		return h_id;
	}
	public void setH_id(String h_id) {
		this.h_id = h_id;
	}
	public String getH_introduce() {
		return h_introduce;
	}
	public void setH_introduce(String h_introduce) {
		this.h_introduce = h_introduce;
	}
	public String getH_other() {
		return h_other;
	}
	public void setH_other(String h_other) {
		this.h_other = h_other;
	}
	public int getH_price() {
		return h_price;
	}
	public void setH_price(int h_price) {
		this.h_price = h_price;
	}
	public String getH_title() {
		return h_title;
	}
	public void setH_title(String h_title) {
		this.h_title = h_title;
	}
	public float getH_star() {
		return h_star;
	}
	public void setH_star(float h_star) {
		this.h_star = h_star;
	}
	public String getH_address1() {
		return h_address1;
	}
	public void setH_address1(String h_address1) {
		this.h_address1 = h_address1;
	}
	public String getH_address2() {
		return h_address2;
	}
	public void setH_address2(String h_address2) {
		this.h_address2 = h_address2;
	}
	public String getH_address3() {
		return h_address3;
	}
	public void setH_address3(String h_address3) {
		this.h_address3 = h_address3;
	}
	public String getH_address4() {
		return h_address4;
	}
	public void setH_address4(String h_address4) {
		this.h_address4 = h_address4;
	}
	public String getH_zipcode() {
		return h_zipcode;
	}
	public void setH_zipcode(String h_zipcode) {
		this.h_zipcode = h_zipcode;
	}
	public int getH_guests() {
		return h_guests;
	}
	public void setH_guests(int h_guests) {
		this.h_guests = h_guests;
	}
	public boolean isH_grant() {
		return h_grant;
	}
	public void setH_grant(boolean h_grant) {
		this.h_grant = h_grant;
	}
	public String getH_facilities() {
		return h_facilities;
	}
	public void setH_facilities(String h_facilities) {
		this.h_facilities = h_facilities;
	}
	public String getH_type() {
		return h_type;
	}
	public void setH_type(String h_type) {
		this.h_type = h_type;
	}
	public String getH_type2() {
		return h_type2;
	}
	public void setH_type2(String h_type2) {
		this.h_type2 = h_type2;
	}
	public String getH_rules() {
		return h_rules;
	}
	public void setH_rules(String h_rules) {
		this.h_rules = h_rules;
	}
	public String getH_res_not_possible_date() {
		return h_res_not_possible_date;
	}
	public void setH_res_not_possible_date(String h_res_not_possible_date) {
		this.h_res_not_possible_date = h_res_not_possible_date;
	}
	public String getH_potos() {
		return h_potos;
	}
	public void setH_potos(String h_potos) {
		this.h_potos = h_potos;
	}
	public String getH_date() {
		return h_date;
	}
	public void setH_date(String h_date) {
		this.h_date = h_date;
	}
	public double getH_mapX() {
		return h_mapX;
	}
	public void setH_mapX(double h_mapX) {
		this.h_mapX = h_mapX;
	}
	public double getH_mapY() {
		return h_mapY;
	}
	public void setH_mapY(double h_mapY) {
		this.h_mapY = h_mapY;
	}

	
}
