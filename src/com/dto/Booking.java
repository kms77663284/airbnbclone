package com.dto;

public class Booking {
	private int b_idx;
	private int b_hidx;
	private String b_uid;
	private String b_uname;
	private String b_hname;
	private String b_uphone;
	private String b_hid;
	private String b_checkin;
	private String b_checkout;
	private String b_date;
	private int b_price;
	private boolean b_deposit;
	private int b_guests;
	private String b_msg;
	private String b_htitle;
	private String b_card;
	private String b_card_info;
	private String b_address;
	public int getB_idx() {
		return b_idx;
	}
	public void setB_idx(int b_idx) {
		this.b_idx = b_idx;
	}
	public int getB_hidx() {
		return b_hidx;
	}
	public void setB_hidx(int b_hidx) {
		this.b_hidx = b_hidx;
	}
	public String getB_uid() {
		return b_uid;
	}
	public void setB_uid(String b_uid) {
		this.b_uid = b_uid;
	}
	public String getB_hid() {
		return b_hid;
	}
	public void setB_hid(String b_hid) {
		this.b_hid = b_hid;
	}
	public String getB_checkin() {
		return b_checkin;
	}
	public void setB_checkin(String b_checkin) {
		this.b_checkin = b_checkin;
	}
	public String getB_checkout() {
		return b_checkout;
	}
	public void setB_checkout(String b_checkout) {
		this.b_checkout = b_checkout;
	}
	public String getB_date() {
		return b_date;
	}
	public void setB_date(String b_date) {
		this.b_date = b_date;
	}
	public int getB_price() {
		return b_price;
	}
	public void setB_price(int b_price) {
		this.b_price = b_price;
	}
	public boolean isB_deposit() {
		return b_deposit;
	}
	public void setB_deposit(boolean b_deposit) {
		this.b_deposit = b_deposit;
	}
	public int getB_guests() {
		return b_guests;
	}
	public void setB_guests(int b_guests) {
		this.b_guests = b_guests;
	}
	public String getB_msg() {
		return b_msg;
	}
	public void setB_msg(String b_msg) {
		this.b_msg = b_msg;
	}
	public String getB_htitle() {
		return b_htitle;
	}
	public void setB_htitle(String b_htitle) {
		this.b_htitle = b_htitle;
	}
	public String getB_card() {
		return b_card;
	}
	public void setB_card(String b_card) {
		this.b_card = b_card;
	}
	public String getB_card_info() {
		return b_card_info;
	}
	public void setB_card_info(String b_card_info) {
		this.b_card_info = b_card_info;
	}
	public String getB_address() {
		return b_address;
	}
	public void setB_address(String b_address) {
		this.b_address = b_address;
	}
	public String getB_uname() {
		return b_uname;
	}
	public void setB_uname(String b_uname) {
		this.b_uname = b_uname;
	}
	public String getB_hname() {
		return b_hname;
	}
	public void setB_hname(String b_hname) {
		this.b_hname = b_hname;
	}
	public String getB_uphone() {
		return b_uphone;
	}
	public void setB_uphone(String b_uphone) {
		this.b_uphone = b_uphone;
	}
	
	
}