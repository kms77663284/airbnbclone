package com.dto;

public class Count {
	private int u_count;
	private int h_count;
	private int b_count;
	private int u_tcount;
	private int h_tcount;
	private int b_tcount;
	public int getU_count() {
		return u_count;
	}
	public void setU_count(int u_count) {
		this.u_count = u_count;
	}
	public int getH_count() {
		return h_count;
	}
	public void setH_count(int h_count) {
		this.h_count = h_count;
	}
	public int getB_count() {
		return b_count;
	}
	public void setB_count(int b_count) {
		this.b_count = b_count;
	}
	public int getU_tcount() {
		return u_tcount;
	}
	public void setU_tcount(int u_tcount) {
		this.u_tcount = u_tcount;
	}
	public int getH_tcount() {
		return h_tcount;
	}
	public void setH_tcount(int h_tcount) {
		this.h_tcount = h_tcount;
	}
	public int getB_tcount() {
		return b_tcount;
	}
	public void setB_tcount(int b_tcount) {
		this.b_tcount = b_tcount;
	}
	

}
