package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dto.Hotel;

public class BookmarkDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static BookmarkDAO instance;
	
	private BookmarkDAO(){
		conn = ConnectionDAO.getConnection();
	}
	
	public static BookmarkDAO getInstance() {
		if (instance == null) instance = new BookmarkDAO();
		return instance;
	}
	//--------------------------
	public void addbookmark(int h_idx, String u_id) {
		sql = "insert into bookmark (u_id, h_idx) values(?,?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, u_id);
			pstmt.setInt(2, h_idx);
			pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	public List<Hotel> bookmarkList(String u_id) {
		sql = "select * from bookmark where u_id=?";
		 
		List<Hotel> list = new ArrayList<Hotel>(); 
		try { 	
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, u_id);
			rs=pstmt.executeQuery();
			
		
			while(rs.next()) {
				HotelDAO dao = HotelDAO.getInstance();
				Hotel hotel = new Hotel();
				String h_idx = Integer.toString(rs.getInt("h_idx"));
				hotel = dao.selectHotelInfo(h_idx);
				list.add(hotel);
			}
			
		
		}catch (SQLException e) {
			e.printStackTrace();
		}	
		
		return list;
	}
	
	public Boolean bookmarkCehck(int h_idx,String u_id) {
		sql ="select f_idx from bookmark where h_idx=? AND u_id=?";
		try {
		pstmt = conn.prepareStatement(sql);
		pstmt.setInt(1, h_idx);
		pstmt.setString(2, u_id);
		rs = pstmt.executeQuery();
		
		if(rs.next()) {
			System.out.println(h_idx+"호텔"+u_id+"가 있음 트루");
			return true;
		}
		
		System.out.println(h_idx+"호텔"+u_id+"가 없음 펄스");
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public void deletebookmark(int h_idx,String u_id) {
		sql ="delete from bookmark where h_idx=? AND u_id=?";
		
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, h_idx);
			pstmt.setString(2, u_id);
			pstmt.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
