package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RuleDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static RuleDAO instance;
	
	private RuleDAO(){
		conn = ConnectionDAO.getConnection();
	}
	
	public static RuleDAO getInstance() {
		if (instance == null) instance = new RuleDAO();
		return instance;
	}
	
	public void SetRule(String rule) {
		try {
			sql = "INSERT INTO rules VALUES(?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, rule);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void DelRule(String rule) {
		try {
			sql = "DELETE FROM rules WHERE rules_name=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, rule);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public List<String> selectRules(){
		sql = "select * from rules";
		List<String> list = new ArrayList<String>();
		try {
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				list.add(rs.getString("rules_name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
		
	}
}
