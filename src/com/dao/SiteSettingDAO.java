package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.dto.SiteSetting;

public class SiteSettingDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static SiteSettingDAO instance;
	
	public SiteSettingDAO() {
		conn = ConnectionDAO.getConnection();
	}
	public static SiteSettingDAO getInstance() {
		if(instance == null) {
			instance = new SiteSettingDAO();
		}
		return instance;
	}
	private void close(PreparedStatement pstmt, ResultSet rs) {
		try { if(pstmt !=null) {pstmt.close();}
		}catch(Exception e) {e.printStackTrace();}
		try {if(rs != null) {rs.close();}
		}catch(Exception e) {e.printStackTrace();}
	}	
	
	public SiteSetting getSiteSetting(SiteSetting siteSetting) {
		SiteSetting tmp = null;
		try {
			sql = "SELECT * FROM sitesetting";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				tmp = new SiteSetting();
				tmp.setS_api1(rs.getString("s_api1"));
				tmp.setS_api2(rs.getString("s_api2"));
				tmp.setS_api3(rs.getString("s_api3"));
				tmp.setS_api4(rs.getString("s_api4"));
				tmp.setS_api5(rs.getString("s_api5"));
				tmp.setS_api6(rs.getString("s_api6"));
				tmp.setS_facebook(rs.getString("s_facebook"));
				tmp.setS_googleStore(rs.getString("s_googleStore"));
				tmp.setS_instagram(rs.getString("s_instagram"));
				tmp.setS_intro(rs.getString("s_intro"));
				tmp.setS_itunesStore(rs.getString("s_itunesStore"));
				tmp.setS_logo(rs.getString("s_logo"));
				tmp.setS_main(rs.getString("s_main"));
				tmp.setS_max(rs.getInt("s_max"));
				tmp.setS_min(rs.getInt("s_min"));
				tmp.setS_ver(rs.getFloat("s_ver"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}
		return tmp;
	}
	
	public void setJoin(SiteSetting siteSetting) {
		try {
			sql = "UPDATE sitesetting set s_facebook = ?, s_instagram = ?, s_googleStore = ?, s_itunesStore = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, siteSetting.getS_facebook());
			pstmt.setString(2, siteSetting.getS_instagram());
			pstmt.setString(3, siteSetting.getS_googleStore());
			pstmt.setString(4, siteSetting.getS_itunesStore());
			pstmt.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}
	}
	public void setSite(SiteSetting siteSetting) {
		try {
			sql = "UPDATE sitesetting set s_ver = ?, s_intro = ?, s_min = ?, s_max = ?, s_logo = ?, s_main = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setFloat(1, siteSetting.getS_ver());
			pstmt.setString(2, siteSetting.getS_intro());
			pstmt.setInt(3, siteSetting.getS_min());
			pstmt.setInt(4, siteSetting.getS_max());
			pstmt.setString(5, siteSetting.getS_logo());
			pstmt.setString(6, siteSetting.getS_main());
			pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}
	}
	public void setApi(SiteSetting siteSetting) {
		try {
			sql = "UPDATE sitesetting set s_api1 = ?, s_api2 = ? , s_api3 = ?, s_api4 = ?, s_api5 = ? , s_api6 = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, "s_api1");
			pstmt.setString(2, "s_api2");
			pstmt.setString(3, "s_api3");
			pstmt.setString(4, "s_api4");
			pstmt.setString(5, "s_api5");
			pstmt.setString(6, "s_api6");
			pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}
	}
}
