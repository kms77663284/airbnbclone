package com.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dto.User;
import com.dao.DashboardDAO;
import com.dto.Count;
@WebServlet("/dashboard")
public class dashboard extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public dashboard() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		DashboardDAO ddo  = DashboardDAO.getInstance();
		
		Count tmp = ddo.dashboard();
		
		request.setAttribute("count", tmp);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("Admin/dashboard.jsp");
		dispatcher.forward(request, response);
	}

}
