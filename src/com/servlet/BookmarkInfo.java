package com.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.BookmarkDAO;
import com.dto.Hotel;
import com.dto.User;

/**
 * Servlet implementation class BookmarkInfo
 */
@WebServlet("/BookmarkInfo")
public class BookmarkInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookmarkInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession();
		
		User user = (User)session.getAttribute("user");
		BookmarkDAO dao = BookmarkDAO.getInstance();
		List<Hotel> list = dao.bookmarkList(user.getU_id());
		request.setAttribute("hotelList", list);  //bookmarklist.jps로 보내는거
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("bookmarklist.jsp");
		dispatcher.forward(request, response);
	}

}
