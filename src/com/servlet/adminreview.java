package com.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.ReviewDAO;
import com.dto.Review;

/**
 * Servlet implementation class adminreview
 */
@WebServlet("/adminreview")
public class adminreview extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public adminreview() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ReviewDAO rdo = ReviewDAO.getInstance();
		String search = request.getParameter("search");
		String sel = request.getParameter("sel");
		String tmp = request.getParameter("page");
		int page = tmp!=null?Integer.parseInt(tmp):1;
		int pagecut = 10;
		int reviewnum = rdo.CountReview(search, sel);
		request.setAttribute("reviewnum", reviewnum);
		ArrayList<Review> list = rdo.getlist(search, sel, page, pagecut);
		request.setAttribute("list", list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("Admin/review.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ReviewDAO rdo = ReviewDAO.getInstance();
		rdo.delReview(request.getParameter("rid"));
	}
}
