package com.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;


@WebServlet("/FileUpload")
public class FileUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public FileUpload() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProsess(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doProsess(request, response);
	}

	private void doProsess(HttpServletRequest request, HttpServletResponse response) {
		String h_idx = request.getParameter("h_idx");
		
		String h_title = "";
		String h_introduce = "";
		String h_other = "";
		
		

		//업로드용 폴더이름 
		String saveFolder="images/hotel/"+h_idx;
		String encType ="utf-8";
		int maxSize=5*1024*1024;
		String realFolder="";
//		//jspmain2-context라고한다 서버에서(서블릿)어디에 어느폴더에 서블릿 변환되니?
		ServletContext context = this.getServletContext();
//		
//		//서블릿상의 upload폴더 경로를 가져온다
		realFolder = context.getRealPath(saveFolder);
		//System.out.println(realFolder);
//		//콘솔 /브라우저에 실제경로 출력
//		System.out.println("실제 서블릿상 경로 :"+realFolder);
//		//out.println("실제 서블릿상 경로 : " + realFolder);
//		
		//파일을 받아와 업로드한다.
		MultipartRequest multi =null;
//		
		try{
			multi = new MultipartRequest(request,realFolder,maxSize,encType,new DefaultFileRenamePolicy());
//			
			Enumeration en = multi.getParameterNames();
			
			en = multi.getFileNames();
			int i=5;
			while(en.hasMoreElements()){
				String name = (String) en.nextElement(); //파라미터 네임 가져옴
				//String originFile = multi.getOriginalFileName(name);
				//String systemFile = multi.getFilesystemName(name);
				//String fileType = multi.getContentType(name);
				
				h_title= multi.getParameter("h_title");
				h_introduce = multi.getParameter("h_introduce");
				h_other = multi.getParameter("h_other");
				
				
				File file = multi.getFile(name);
//				
//				//out.print("파라미터 이름: "+name+"<br>");
//				//out.print("원본이름: "+originFile+"<br>");
//				//out.print("시스템 이름: "+systemFile+"<br>");
//				//out.print("파일 타입이름: "+fileType+"<br>");
				if(file !=null) {
					String type = file.getName().substring(file.getName().lastIndexOf("."));
					String editName ="/h"+h_idx+"image"+i+type;
					
					
					File newFile = new File(realFolder+editName);
					if(!newFile.isFile()) {
						file.renameTo(newFile);
					}else {
						newFile.delete();
						file.renameTo(newFile);
					}
					
					//바꾸고 같은 이름의 예전걸 지운다.
					String dirPath =realFolder;
					System.out.println(dirPath);
					File img = new File(dirPath);
					File[] fList = img.listFiles();
					if(img.isDirectory()) {
						
						//System.out.println("디렉토리있음");
						if(fList.length>0) {
							
							for(int j = 0; j<fList.length;j++) {
								if(fList[j].isFile()) {
									//이름이 같은걸 찾는다
									if(editName.substring(1,editName.lastIndexOf(".")).equalsIgnoreCase(fList[j].getName().substring(0,fList[j].getName().indexOf(".")))) {
										//확장자가 다른지 확인한ek다.
										if(!editName.substring(editName.lastIndexOf(".")).equalsIgnoreCase(fList[j].getName().substring(fList[j].getName().indexOf(".")))) {
											System.out.println("확장자가 다르다 기존걸 지운다.");
											fList[j].delete();
										}
									}	
								}
								
							}
						}
					}
					
					
					//System.out.println(file.getName());
				}
				
				
				
				
				i--;
			}
//			
//			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		request.setAttribute("h_title", h_title);
		request.setAttribute("h_idx", h_idx);
		request.setAttribute("h_other", h_other);
		request.setAttribute("h_introduce", h_introduce);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("HostTitle.jsp");
		
		try {
			dispatcher.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
