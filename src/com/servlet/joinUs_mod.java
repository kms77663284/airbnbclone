package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.SiteSettingDAO;
import com.dto.SiteSetting;

@WebServlet("/joinUs_mod")
public class joinUs_mod extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public joinUs_mod() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String s_facebook = request.getParameter("s_facebook");
		String s_instagram = request.getParameter("s_instagram");
		String s_googleStore = request.getParameter("s_googleStroe");
		String s_itunesStore = request.getParameter("s_intunesStore");
		
		SiteSettingDAO dao = SiteSettingDAO.getInstance();
		SiteSetting tmp = new SiteSetting();
		tmp.setS_facebook(s_facebook);
		tmp.setS_googleStore(s_googleStore);
		tmp.setS_instagram(s_instagram);
		tmp.setS_itunesStore(s_itunesStore);
		
		response.sendRedirect("Admin/joinUs.jsp");
		
	}

}
