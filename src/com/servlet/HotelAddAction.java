package com.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.jasper.tagplugins.jstl.core.Out;

import com.dao.HotelDAO;
import com.dto.User;


@WebServlet("/HotelAddAction")
public class HotelAddAction extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public HotelAddAction() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 
		doprosess(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doprosess(request,response);
		
	}

	private void doprosess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		RequestDispatcher dispatcher =null;
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("user");
		
		
		
		String selectH_idx = request.getParameter("selectH_idx");
		if(request.getParameter("selectH_idx")!=null) {
			
	

		
			if(selectH_idx.equals("0")){
				HotelDAO dao = HotelDAO.getInstance();
				String h_idx = dao.insertHotel(u.getU_id());
				request.setAttribute("h_idx", h_idx);
				
				dispatcher = request.getRequestDispatcher("HostRoom.jsp");
		        
			}else{
				request.setAttribute("h_idx", selectH_idx);
				dispatcher = request
		                .getRequestDispatcher("HotelInfoAction");
		        
			}
		}else {
			dispatcher = request.getRequestDispatcher("HotelMyList");
			
		}

		dispatcher.forward(request, response);
		
	}

}
