package com.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.HotelDAO;
import com.dto.Hotel;
import com.dto.User;

/**
 * Servlet implementation class HostMyHotel
 */
@WebServlet("/HostMyHotel")
public class HostMyHotel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HostMyHotel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProsess(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProsess(request,response);
	}
	private void doProsess(HttpServletRequest request, HttpServletResponse response) {
		try {
			RequestDispatcher dispatcher = null;
			//String url = request.getRequestURL().substring((request.getRequestURL().lastIndexOf("/")));
			//String referer = request.getHeader("referer");
					//referer = referer.substring(referer.lastIndexOf("/")).substring(1); //무슨페이지에서 왔는지
					//System.out.println(referer);
			
			
			HttpSession session = request.getSession();
			User u = (User)session.getAttribute("user");
			List<Hotel> list = new ArrayList<Hotel>();
			int pageNum=1;
			if(request.getParameter("pageNum")!=null) {
				pageNum = Integer.parseInt(request.getParameter("pageNum").toString());//현제 몇페이지인지
			}
			
			
			String room="";
			String detail="";
			String booking ="";
			String check = "chekc";
			
			int pc; //페이지 카운트 숙소가 10개면 5로 나누어서 2| 1,2
			
			
			
			room="display: block;;";
			detail="display: none;";
			booking="display: none;";
			request.setAttribute("room", room);
			request.setAttribute("detail",detail);
			request.setAttribute("booking", booking);
			HotelDAO dao = HotelDAO.getInstance();
			
			list = dao.pagingHotelList(u.getU_id(),pageNum);
			int count = dao.CountHotel(u.getU_id());
			int mod = count % 5;
			pc = count/5;
			if(mod!=0) {pc++;}
			
			if(pc<1) {pc=1;}
			request.setAttribute("pc", pc);
			request.setAttribute("HotelList", list);
			request.setAttribute("check", check);
			request.setAttribute("pageNum", pageNum);
	
			dispatcher = request.getRequestDispatcher("HostMyPage.jsp");
			dispatcher.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
