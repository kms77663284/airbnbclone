package com.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.HotelDAO;
import com.dto.Hotel;
import com.dto.Review;

/**
 * Servlet implementation class adminroom
 */
@WebServlet("/adminroom")
public class adminroom extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public adminroom() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		HotelDAO hdo = HotelDAO.getInstance();
		
		String search = request.getParameter("search");
		String sel = request.getParameter("sel");
		String tmp = request.getParameter("page");
		int page = tmp!=null?Integer.parseInt(tmp):1;
		int pagecut = 10;
		int hotelnum = hdo.CountHotel(search, sel);
		request.setAttribute("hotelnum", hotelnum);
		ArrayList<Hotel> list = hdo.getlist(search, sel, page, pagecut);
		request.setAttribute("list", list);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("Admin/room.jsp?page="+page);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HotelDAO hdo = HotelDAO.getInstance();
		
		String h_idx = request.getParameter("h_idx");
		String met = request.getParameter("met");
		
		if (met.contentEquals("del")) {
			hdo.DelHotel(Integer.parseInt(h_idx));
		}else{
			hdo.PermitHotel(Integer.parseInt(h_idx));
		}
	}

}
