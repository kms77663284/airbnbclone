package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.SiteSettingDAO;
import com.dto.SiteSetting;

@WebServlet("/siteSetting_mod")
public class siteSetting_mod extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public siteSetting_mod() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		float s_ver = Float.parseFloat(request.getParameter("s_ver"));
		String s_intro = request.getParameter("s_intro");
		String s_logo = request.getParameter("s_logo");
		int s_min = Integer.parseInt(request.getParameter("s_min"));
		int s_max = Integer.parseInt(request.getParameter("s_max"));
		String s_main = request.getParameter("s_main");
		
		SiteSettingDAO dao = SiteSettingDAO.getInstance();
		
		SiteSetting tmp = new SiteSetting();
		tmp.setS_ver(s_ver);
		tmp.setS_intro(s_intro);
		tmp.setS_logo(s_logo);
		tmp.setS_min(s_min);
		tmp.setS_max(s_max);
		tmp.setS_main(s_main);
		dao.setSite(tmp);
		
		response.sendRedirect("Admin/siteSetting.jsp");		
	}

}
