package com.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.FacilitiesDAO;

/**
 * Servlet implementation class amenities
 */
@WebServlet("/amenities")
public class amenities extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public amenities() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FacilitiesDAO fd = FacilitiesDAO.getInstance();
		
		ArrayList<String> list = fd.SelectFacilities();
		
		request.setAttribute("list", list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("Admin/amenities.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String fac = request.getParameter("fac");
		String cmd = request.getParameter("met");
		FacilitiesDAO fd = FacilitiesDAO.getInstance();
		if (cmd.equals("add")) fd.AddFacilities(fac);
		else fd.DelFacilities(fac.replaceAll("\"", ""));
	}
}
