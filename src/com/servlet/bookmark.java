package com.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dto.Hotel;
import com.dto.Review;
import com.dto.User;
import com.dao.BookmarkDAO;
import com.dao.HotelDAO;
import com.dao.ReviewDAO;

/**
 * Servlet implementation class bookmark
 */
@WebServlet("/bookmark")
public class bookmark extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public bookmark() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String h_idx = request.getParameter("h_idx");
		HttpSession session = request.getSession();
		
		User user = (User)session.getAttribute("user");
		if (user == null) {
			System.out.println("세션유저가 널이다");
			response.sendRedirect("Login.jsp");
		}else {
		
			BookmarkDAO dao = BookmarkDAO.getInstance();
			
			Boolean check = dao.bookmarkCehck(Integer.parseInt(h_idx),user.getU_id());
			
			if(!check) {
				dao.addbookmark(Integer.parseInt(h_idx), user.getU_id());
				System.out.println(user.getU_id()+"가"+h_idx+"번 북마크함");
			}else{
				dao.deletebookmark(Integer.parseInt(h_idx),user.getU_id());
				System.out.println(user.getU_id()+"가"+h_idx+"번 북마크 삭제함");
			}
			
			
			
			
			HotelDAO h_dao = HotelDAO.getInstance();
			ReviewDAO r_dao = ReviewDAO.getInstance();
			BookmarkDAO b_dao = BookmarkDAO.getInstance();
			
			ArrayList<Review> review = new ArrayList<Review>();
			review=r_dao.getReviews(h_idx);
	
			
			if(review.size()>0) {
				request.setAttribute("review", review);
			}
			String referer = request.getHeader("referer");
			referer = referer.substring(referer.lastIndexOf("/")).substring(1); //무슨페이지에서 왔는지
			System.out.println(referer+"에서 왔습니다.");
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("Detail.jsp");
			if(referer.equals("bookmark")||referer.substring(0,10).toString().equals("DetailInfo")) {
				Hotel hotel = h_dao.selectHotelInfo(h_idx);
				request.setAttribute("hotel", hotel);
			}
			if(referer.equals("BookmarkInfo")) {
				List<Hotel> list = b_dao.bookmarkList(user.getU_id());
				request.setAttribute("hotelList", list);
				dispatcher = request.getRequestDispatcher("bookmarklist.jsp");
			}
			dispatcher.forward(request, response);
		}
		
		
	}

}
