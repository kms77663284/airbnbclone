package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.ReviewDAO;
import com.dto.Review;
import com.dto.User;

/**
 * Servlet implementation class UploadReview
 */
@WebServlet("/UploadReview")
public class UploadReview extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadReview() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		ReviewDAO rao = ReviewDAO.getInstance();
		User user = (User)request.getSession().getAttribute("user");
		Review rv = new Review();
		String h_idx = request.getParameter("h_idx");
		String star = request.getParameter("star");
		String content = request.getParameter("content");
		rv.setR_uid(user.getU_id());
		rv.setR_hidx(Integer.parseInt(h_idx));
		rv.setR_content(content);
		rv.setR_star(Double.parseDouble(star));
		rao.addReview(rv);
		response.sendRedirect("mypage_res?page=1");
	}
	
}
