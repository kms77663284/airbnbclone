package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserDAO;
import com.dto.User;

@WebServlet("/user_del")
public class user_del extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public user_del() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		UserDAO dao = UserDAO.getInstance();
	
		String u_id = (String) request.getParameter("u_id");
		
		User user = new User();
		user.setU_id(u_id);
		dao.delUser(user);
		
		response.sendRedirect("Admin/user.jsp");
	}

}
