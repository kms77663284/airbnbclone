package com.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.HotelDAO;
import com.dto.Hotel;
import com.dto.User;

/**
 * Servlet implementation class HotelMyList
 */
@WebServlet("/HotelMyList")
public class HotelMyList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HotelMyList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doprosess(request,response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doprosess(request,response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void doprosess(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		User u = (User)session.getAttribute("user");
		if(u == null) {
			response.sendRedirect("Login.jsp");
		}

		HotelDAO dao = HotelDAO.getInstance();
		List<Hotel> list = new ArrayList<Hotel>();
		list=dao.HotelList(u.getU_id());
		
//		session.setAttribute("HotelList", list);
		request.setAttribute("HotelList", list);
		
		
		RequestDispatcher dispatcher =
				request.getRequestDispatcher("HostMain.jsp");
		
		dispatcher.forward(request, response);
		
		
		
		
		
	
	}


}
