package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.SiteSettingDAO;
import com.dto.SiteSetting;

@WebServlet("/api_mod")
public class api_mod extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public api_mod() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String s_api1 = request.getParameter("s_api1");
		String s_api2 = request.getParameter("s_api2");
		String s_api3 = request.getParameter("s_api3");
		String s_api4 = request.getParameter("s_api4");
		String s_api5 = request.getParameter("s_api5");
		String s_api6 = request.getParameter("s_api6");
		
		SiteSettingDAO dao = SiteSettingDAO.getInstance();
		SiteSetting tmp = new SiteSetting();
		tmp.setS_api1(s_api1);
		tmp.setS_api2(s_api2);
		tmp.setS_api3(s_api3);
		tmp.setS_api4(s_api4);
		tmp.setS_api5(s_api5);
		tmp.setS_api6(s_api6);
		dao.setApi(tmp);
		
		response.sendRedirect("Admin/api.jsp");
	}

}
