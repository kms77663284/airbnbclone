<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.Hotel" %>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
	request.setCharacterEncoding("utf-8");

%>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     
<jsp:useBean id="hotel" class="com.dto.Hotel"></jsp:useBean>
<jsp:setProperty property="*" name="hotel"/>

<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">

                <p class="p_start">2단계 : 상세 정보를 제공해주세요.</p>
                <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 55%"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6 offset-1 div-margin-top">
                        <p class="p_title">게스트에게 숙소에 대해 설명해주세요.</p>
                        <p class="p_content">숙소에 대해 간략히 설명해주세요. 숙소와 주변지역에 대한 정보에서 시작해 게스트와 어떻게 소통하고 싶은지 등의 내용을 적어주세요.</p>
                        <form action="HotelAddEdit" method="post" name="HostForm">
                                    
                            <div class="form-group">
                                <div class="form-group">
                                    <textarea name="h_introduce" class="form-control" id="exampleTextarea" rows="5" placeholder="인테리어, 채광, 주변 정보 등을 설명하세요."><%if(hotel.getH_introduce().equals("null")){}else{ %><%=hotel.getH_introduce()%><%} %></textarea>
                                </div>

                                <p class="p_title">정보를 추가하시겠습니까?</p>
                                <p class="p_content">자세한 내용을 공유하시려면 아래의 추가 필드를 이용하세요.</p>
                                <div class="form-group">
                                        <textarea name="h_other" class="form-control" id="exampleTextarea" rows="5" placeholder="근처 대중교통수단, 운전정보, 편리한도보 경로 등 도시와 동네 팁을 설명해주세요."><%if(hotel.getH_other().equals("null")){}else{ %><%=hotel.getH_other()%><%} %></textarea>
                                    </div>
                               </div> 

								<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                            	<input type="hidden" name="h_title" value="<%=hotel.getH_title()%>">
                            	<input type="hidden" name="step" value="2">
                      
                        </form>
                    </div>

            </div>

			
		</div>

<jsp:include page="HostFooter.jsp" />
</div>
        
        



</body>
</html>























