<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "amenities"); %>
<%@ page import="java.util.ArrayList" %>
<%
	ArrayList<String> list = (ArrayList<String>)request.getAttribute("list");
	int cnt = list.size();
	int sel = cnt % 3;
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>amenities</title>
<link href="<%=request.getContextPath()%>/css/admin.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/dashboard.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/paging.css"
	rel="stylesheet">
</head>

<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>


			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">편의시설설정</li>
				</ol>

				<div class="table-responsive">
					<div class="tri">
						<table class="table table-striped">
							<thead> 
								<tr>
									<th>편의 시설</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="tbody1">
								<%for (int i=0;i < (cnt/3 + (sel>0?1:0)); i++){%>
								<tr>
									<td><%=list.get(i)%></td>
									<td align="right">
										<form action="">
											<button class="btn btn-primary btn-sm">삭제</button>
										</form>
									</td>
								</tr>
								<%}%>
							</tbody>
						</table>
					</div>
					<div class="tri">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>편의 시설</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="tbody2">
								<tr>
									<td>수건</td>
									<td align="right"><a class="btn btn-primary btn-sm"
										href="#" role="button">삭제</a></td>
								</tr>
								<%for (int i=(cnt/3 + (sel>0?1:0));i < ((cnt/3)*2 + (sel>1?1:0)); i++){%>
								<tr>
									<td><%=list.get(i)%></td>
									<td align="right">
										<form action="">
											<button class="btn btn-primary btn-sm">삭제</button>
										</form>
									</td>
								</tr>
								<%}%>
							</tbody>
						</table>
					</div>
					<div class="tri">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>편의 시설</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="tbody3">
								<%for (int i=((cnt/3)*2 + (sel>1?1:0));i < cnt; i++){%>
								<tr>
									<td><%=list.get(i)%></td>
									<td align="right">
										<form action="">
											<button class="btn btn-primary btn-sm">삭제</button>
										</form>
									</td>
								</tr>
								<%}%>
								<tr id="add">
									<td colspan="2" align="right">
										<form action="" class="fmad">
											<input class="form-control mr-sm-2" type="text" id="fac">
											<button class="btn btn-secondary my-2 my-sm-0">추가</button>
										</form>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	$('.btn-secondary').click(function(){
		var f = $('#fac').val();
		$.ajax({
			url: 'amenities',
			type:'POST',
			data: {
				fac: f,
				met: 'add'
			}
		});
	});
	$('.btn-primary').click(function(){
		var f = $(this).parent().parent().prev().text();
		$.ajax({
			url: 'amenities',
			type:'POST',
			data:{
				fac: f,
				met: 'del'
			}
		});
	});
</script>
</html>