<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "review"); %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dto.Review" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>review</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>
<%
	ArrayList<Review> list = (ArrayList<Review>)request.getAttribute("list");
	list = list==null?new ArrayList<Review>():list;
	String curpage = request.getParameter("page");
	int reviewnum = (int)request.getAttribute("reviewnum");
	int pagenum = reviewnum / 10 + (reviewnum%10>0?1:0);
	int cp = (curpage==null||curpage.equals(""))?1:Integer.parseInt(curpage);
%>
<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">리뷰 관리</li>
				</ol>
				<div>
					<form class="form-inline my-2 my-lg-0 searching" id="r_search" action="adminreview" method="get">
						<input type="hidden" name="page" id="c_page" value="<%=cp%>">
						<select name="catg" class="form-control" id="exampleSelect1" name="sel">
							<option value="null" selected>선택</option>
							<option value="r_idx">리뷰 번호</option>
							<option value="u_id">유저 아이디</option>
							<option value="h_idx">숙소 번호</option>
							<option value="r_content">리뷰글</option>
							<option value="r_date">리뷰 작성시간</option>
							<option value="r_star">평점</option>
						</select> <input name="search" class="form-control mr-sm-2" type="text" name="search" placeholder="Search">
						<button class="btn btn-secondary my-2 my-sm-0" type="submit">검색</button>
					</form>
				</div>
				<div class="table-responsive">
					<table class="table table-striped revi">
						<thead>
							<tr>
								<th class="first">리뷰 번호</th>
								<th class="second">유저 아이디</th>
								<th class="third">숙소 번호</th>
								<th>리뷰 글</th>
								<th class="forth">리뷰 작성 시간</th>
								<th class="fifth">평점</th>
								<th class="sixth"></th>
							</tr>
						</thead>
						<tbody>
							<%if(list.size()>0){ %>
								<%for (Review rv : list){%>
								<tr>
									<td><%=rv.getR_idx() %></td>
									<td><%=rv.getR_uid() %></td>
									<td><%=rv.getR_hidx() %></td>
									<td><%=rv.getR_content() %></td>
									<td><%=rv.getR_date() %></td>
									<td><%=rv.getR_star() %></td>
									<td>
										<form action="">
											<button class="btn btn-primary btn-sm">삭제</button>
										</form>
									</td>
								</tr>
								<%}%>
							<%} %>
						</tbody>
					</table>
					<div class="paging_box">
						<div class="paging">
							<ul class="pagination">
								<li class="page-item"><a class="page-link" onclick="paging('b')">&laquo;</a></li>
							<%for (int i=cp>3?cp-2:1; i <= ((pagenum>=cp+2)?cp+2:pagenum); i++){%>
								<li class="page-item <%if(i==cp){%>active<%}%>"><a class="page-link" onclick="paging(<%=i%>)"><%=i%> </a></li>
							<%}%>
								<li class="page-item"><a class="page-link" onclick="paging('f')">&raquo;</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	$('.btn-primary').click(function(){
		var rid = $(this).parent().parent().prev().prev().prev().prev().prev().prev().text();
		$.ajax({
			url: 'adminreview',
			type: 'POST',
			data:{
				rid: rid
			}
		});
	});
	
    paging = function(page){
        if (page == 'b'){
        	var p = parseInt($('#c_page').val());
        	if (p > 1){
        		$('#c_page').val(p-1);
        	}
        }
        else if (page == 'f'){
        	var p = parseInt($('#c_page').val());
        	if (p < <%=pagenum%>){
        		$('#c_page').val(p+1);
        	}
        }
        else{
        	$('#c_page').val(page);
        }
        $('#r_search').submit();
	}
</script>
</html>