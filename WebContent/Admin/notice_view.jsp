<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "notice"); %>
<%@ page import="com.dto.Notice"%>
<%
	Notice noticeView = (Notice) request.getAttribute("noticeView");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title><%=noticeView.getN_title()%></title>
<link href="<%=request.getContextPath()%>/css/admin.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/dashboard.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/paging.css"
	rel="stylesheet">
</head>
<body>
<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>


			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a
						href="<%=request.getContextPath()%>/Admin/notice.jsp">공지사항 관리</a>
					</li>
					<li class="breadcrumb-item active">공지사항 view</li>
				</ol>
				<form action="<%=request.getContextPath()%>/notice_regi"
					method="post">
					<div class="notnull">
						<p>
							제목 : <input type="text" name="n_title"
								placeholder="<%=noticeView.getN_title()%>" readonly>
						</p>
						<p>
							작성자 :
							<%=noticeView.getA_id()%>, 조회수 :
							<%=noticeView.getN_vcount()%>
						<p>
						<p>
							작성 시간 :
							<%=noticeView.getN_date()%>
						<p>
						<p>내용 :</p>
						<textarea rows="20" name="n_content"
							placeholder="<%=noticeView.getN_content()%>" readonly></textarea>

					</div>
					<div class="sitesetting">
						<a class="btn btn-primary btn-lg"
							href="<%=request.getContextPath()%>/Admin/notice_mod.jsp?n_idx=<%=noticeView.getN_idx()%>"
							role="button"> 수 정</a> / <a class="btn btn-primary btn-lg"
							href="<%=request.getContextPath()%>/notice_del?n_idx=<%=noticeView.getN_idx()%>"
							role="button"> 삭 제</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>