<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "property"); %>
<%@ page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>property</title>
        <link href="<%=request.getContextPath()%>/css/admin.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/dashboard.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/paging.css" rel="stylesheet">
	</head>
	<%
		ArrayList<String>[] list = (ArrayList<String>[])request.getAttribute("list");
		if (list == null){
			list = new ArrayList[3];
			list[0] = new ArrayList<String>();
			list[1] = new ArrayList<String>();
			list[2] = new ArrayList<String>();
		}
	%>
    <body>
      <%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">숙소 종류,이용 규칙,숙소 유형 설정</li>
                    </ol>

                    <div class="table-responsive">
                        <div class="tri">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="2">숙소 종류</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <%for(String h_type2 : list[1]){ %>
                                	<tr>
                                        <td><%=h_type2 %> </td>
                                        <td align="right">
                                        	<form action="" >
                                        		<input type="hidden" value="type2" name="type">
                                        		<button class="btn btn-primary btn-sm">삭제</button>
                                        	</form>
										</td>
                                    </tr>
                                <%} %>
                                    <tr>
                                        <td colspan="2">
                                            <form action="" class="fmad">
                                            	<input type="hidden" value="type2" name="type">
                                                <input class="form-control mr-sm-2" type="text" name="item">
                                                <button class="btn btn-secondary my-2 my-sm-0" type="submit">추가</button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tri">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="2">이용 규칙</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <%for(String rule : list[2]){ %>
                                	<tr>
                                        <td><%=rule %> </td>
                                        <td align="right">
                                        	<form action="" >
                                        		<input type="hidden" value="rules" name="type">
                                        		<button class="btn btn-primary btn-sm">삭제</button>
                                        	</form>
										</td>
                                    </tr>
                                <%} %>
                                    <tr>
                                        <td colspan="2">
                                            <form action="" class="fmad">
                                            	<input type="hidden" value="rules" name="type">
                                                <input class="form-control mr-sm-2" type="text" name="item">
                                                <button class="btn btn-secondary my-2 my-sm-0" type="submit">추가</button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tri">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="2">숙소 유형</th>
                                    </tr>
                                </thead>
                                <tbody>
 								<%for(String h_type1 : list[0]){ %>
                                	<tr>
                                        <td><%=h_type1 %> </td>
                                        <td align="right">
                                        	<form action="" >
                                        		<input type="hidden" value="type1" name="type">
                                        		<button class="btn btn-primary btn-sm">삭제</button>
                                        	</form>
                                        </td>
                                    </tr>
                                <%} %>
                                    <tr>
                                        <td colspan="2">
                                            <form action="" class="fmad">
                                            	<input type="hidden" value="type1" name="type">
                                                <input class="form-control mr-sm-2" type="text" name="item">
                                                <button class="btn btn-secondary my-2 my-sm-0" type="submit">추가</button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	$('.btn-secondary').click(function(){
		var item = $(this).prev().val();
		var type = $(this).prev().prev().val();
		$.ajax({
			url: 'adminproperty',
			type: 'POST',
			data:{
				met: 'add',
				type: type,
				item: item
			}
		});
	});
	$('.btn-primary').click(function(){
		var item = $(this).parent().parent().prev().text();
		var type = $(this).prev().val();
		$.ajax({
			url: 'adminproperty',
			type: 'POST',
			data:{
				met: 'del',
				item: item,
				type: type
			}
		});
	});
</script>
</html>