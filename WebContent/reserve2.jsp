<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.Hotel" %>
<%@ page import="com.dao.HotelDAO" %>
<%@ page import="com.dto.User" %>
<%
	User user =null;
	
	if(session.getAttribute("user")!=null){
		user= (User)session.getAttribute("user");
	}
	if (user == null){response.sendRedirect("Login.jsp");}
%>
<%
	request.setCharacterEncoding("utf-8");
	String h_idx = request.getParameter("h_idx");
	HotelDAO dao = HotelDAO.getInstance();
	Hotel hotel = dao.selectHotelInfo(h_idx);
	System.out.println(hotel.getH_id());
	String ppl = request.getParameter("ppl");
	String checkin =request.getParameter("checkin");
	String checkout =request.getParameter("checkout");
	String startYYYY = checkin.substring(0,4);
	String startMM = checkin.substring(5,7);
	String startDD = checkin.substring(8,10);
	String endYYYY=checkout.substring(0,4);
	String endMM=checkout.substring(5,7);
	String endDD=checkout.substring(8,10);
	
	
	
	
	long date=Integer.parseInt(request.getParameter("date"));
	
	
%>
<!DOCTYPE html>
<html>
<head>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link
            href="https://fonts.googleapis.com/css?family=Bad+Script&display=swap"
            rel="stylesheet">
        <link
            href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800&display=swap&subset=korean"
            rel="stylesheet">
        <title>KoreaBnb</title>
        <link rel="stylesheet" href="css/reserve2.css">
    </head>
</head>
    <body>
        <header>
            <div>
                <img src="images/logo.png" id="logo_image">
                <nav id="topmenu">
                    <span class="bold1">
                        1. 숙소 이용규칙 확인
                        <span id="n">
                            >
                        </span>
                    </span>
                    <span class="bold1">
                        2. 게스트 정보 입력
                        <span id="n">
                            >
                        </span>
                    </span>
                    <span class="bold2">
                        3. 확인 및 결제</span>
                </nav>
            </div>
        </header>
        
        <div id="container">
        	<form method="post" action="reserve3.jsp" name="sub">
            <div id="rule-box">
                <div id="title">
                    <h2>일행이 있나요?</h2>
                    <div id="opp">
                        <img src="images/reserve_icon.jpg">
                        <span>흔치 않은 기회입니다.</span>
                        이 숙소는 보통 예약이 가득 차 있습니다.
                    </div>
                </div>

                <h3>호스트에게 인사하기</h3>
                <h4><%=hotel.getH_id() %>님에게 간단히 자신을 소개하고 여행 목적에 대해 알려주세요.</h4>
                <p>
                    <textarea name="b_msg" placeholder="<%=hotel.getH_id() %>님, 안녕하세요! 숙소에서 보낼 멋진 <%=date %>박이 기다려집니다!"></textarea>
                </p>
                <div id="button">
                        <input type="hidden" value="<%=request.getParameter("checkin") %>" name="b_checkin">
                		<input type="hidden" value="<%=request.getParameter("checkout") %>" name="b_checkout">
                    	<input type="hidden" value="<%=hotel.getH_idx() %>" name="h_idx">
                    	<input type="hidden" value="<%=date %>" name="date">
                    	<input type="hidden" value="<%=ppl %>" name="ppl">
                    	
                    	<input type="submit" value="동의 후 계속하기">
                </div>
            </div>
            </form>
            <div id="margin-box"></div>
            <div id="pay-box">
                <div id="popup">
                        <div id="popuptitle">
                            <h4><%=hotel.getH_title() %></h4>
                            <h5><%=hotel.getH_address1() %>의 <%=hotel.getH_type2() %>의 <%=hotel.getH_type() %><br><br></h5>
                            <img src="images/icon4_1.jpg" id="popup_img">
                            게스트 <%=ppl %>명<br><br>
                            <img src="images/icon4_2.jpg" id="popup_img">
                            <%=startYYYY %>년 <%=startMM %>월 <%=startDD %>일 -> <%=endYYYY %>년 <%=endMM %>월 <%=endDD %>일
                        </div>
                        <div class="content3">
                            <div class="content3_1 content3_2 content3_3">
                                ₩<%=hotel.getH_price() %> x <%=date%>박 x <%=ppl %>명
                                <span class="money">₩<%=hotel.getH_price() * date * Integer.parseInt(ppl)%></span></div>
                            <div class="content3_1 content3_3">
                                청소비
                                <span class="money">₩10,000</span></div>
                            <div class="content3_1 content3_3">
                                서비스 수수료
                                <span class="money">₩<%=Math.floor((hotel.getH_price() * date)*0.10)%></span></div>
                            <div class="content3_1 content3_3">
                                숙박세와 수수료<span class="money">₩<%=Math.floor((hotel.getH_price() * date)*0.01)%></span></div>
                        </div>
                        <div class="content3">
                            <div class="content3_1 content3_4">
                                총 합계 (KRW)
                                <span class="money">₩<%=Math.floor(10000+hotel.getH_price() * date * Integer.parseInt(ppl) +(hotel.getH_price() * date)*0.10+(hotel.getH_price() * date)*0.01) %></span></div>
                        </div>
                  
                </div>
            </div>
        </div>
        

</body>
</html>