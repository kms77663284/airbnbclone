<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.dto.Hotel" %>
<%@ page import="com.dto.Booking" %>
<%@ page import="java.io.File" %>
<%@ page import="com.dto.User" %>
<%@ page import="com.dto.Booking" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
    request.setCharacterEncoding("utf-8");
    String room = (String)request.getAttribute("room");
    String detail = (String)request.getAttribute("detail");
    String booking = (String)request.getAttribute("booking");
    List<Hotel> hList = new ArrayList<Hotel>();
    List<Booking> bList = new ArrayList<Booking>(); 
    Booking bInfo = new Booking();
    Hotel hotel = new Hotel();
    if(request.getAttribute("HotelList")!=null){
    	hList=(List<Hotel>)request.getAttribute("HotelList");
    }
    if(request.getAttribute("BookingList")!=null){
    	bList =(List<Booking>)request.getAttribute("BookingList");
    }
    if(request.getAttribute("bInfo")!=null&& request.getAttribute("hotel")!=null){
    	bInfo = (Booking)request.getAttribute("bInfo");
    	hotel = (Hotel)request.getAttribute("hotel");
    }
    String check =null;
	if(request.getAttribute("check")!=null){
		check = (String)request.getAttribute("check");
	}
    
    int pc=0;
    if(request.getAttribute("pc")!=null){
    	pc = Integer.parseInt(request.getAttribute("pc").toString()); //페이지카운트 총숙소/5
    }
    
	int pageNum=1; //지금 페이지 번호 
	if(request.getAttribute("pageNum")!=null){
		pageNum =Integer.parseInt(request.getAttribute("pageNum").toString());
	}
	

    %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link  href="<%=request.getContextPath() %>/css/HostMyPage.css" rel="stylesheet">
<script src="<%=request.getContextPath() %>/js/HostMyPage.js"></script>
<link href="<%=request.getContextPath()%>/css/paging.css">
<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
            
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main">
<div class="main">
				<nav id="topmenu">  
                    <ul>
                        <li id="<%if(check==null){%>sel1<%}else{%><%}%>"><a href="HostMyBooking">예약</a></li>
                        <li id="<%if(check!=null){%>sel1<%}else{%><%} %>"><a href="HostMyHotel">숙소</a></li>
                    </ul>
                </nav>
            <div id="room" style="<%=room%>">
                <table class="table">
                    <caption>
                        <span>숙소 <%=hList.size() %> 개</span> <input type="text" placeholder="숙소 이름, 위치 등으로 검색">
                    </caption>
                    <tr>
                        <th>상태</th>
                        <th class="long_td" colspan="2">숙소</th>
                        <th class="short_td">숙소</th>
                        <th>위치</th>
                        <th>최종수정일</th>
                        <th>옵션</th>
                    </tr>
                    <%if(hList.size()==0){ %>
                    	<tr>
                    		<td colspan="6">등록된 숙소가 없습니다.</td>
                    	</tr>
                    <%}else{ %>

			
                    
	                    <%for(Hotel h : hList){ %>
	                     <tr>
	                     <td><%if(!h.isH_grant()){%>등록중<%}else{%>허가완료<%} %></td>
<%
String imgPath="";
String dirPath =request.getRealPath(File.separator)+ "/images/hotel/"+h.getH_idx();
File file = new File(dirPath);
File fList[] = file.listFiles();
if(!file.isDirectory()||fList.length ==0){
	imgPath=request.getContextPath()+"/images/q.gif";
%>
				<td><img id="img1" src="<%=imgPath %>"></td>
<%
}else{
	if(fList.length>0){
		imgPath = request.getContextPath()+"/images/hotel/"+h.getH_idx()+"/"+fList[0].getName();
%>
	<td><img id="img1" src="<%=imgPath %>"></td>	
<%
     }
}
%>
	                        <td><%=h.getH_title() %></td>
	                        <td><%=h.getH_address1()+h.getH_address2()+h.getH_address3() %></td>
	                        <td><%=h.getH_date() %></td>
	                        <td><a href="HotelInfoAction?h_idx=<%=h.getH_idx()%>">수정</a> <a href="HotelDelete?h_idx=<%=h.getH_idx() %>" onclick="delchk()">삭제</a></td>
	                    </tr>
	                    <%} %>
					<%} %>
                </table>
                <ul class="pagination ">
					<li class="page-item"><a class="page-link" onclick="hPaging('b')">&laquo;</a></li>
				<%for (int i = 1;i<=pc;i++){%>
					<li class="page-item <%if(i==pageNum){%>active<%}%>"><a class="page-link" onclick="hPaging(<%=i%>)"><%=i%> </a></li>
				<%}%>
					<li class="page-item"><a class="page-link" onclick="hPaging('f')">&raquo;</a></li>
				</ul>
            </div>
            <div id="detail" style="<%=detail%>">
                <table class="table" id="b_table">
                    <caption>
                        <h>예약조회</h>
                    </caption>
                    <tr class="bottomline title">
                        <th colspan="4">예약자 정보</th>
                    </tr>
                    <tr>
                        <td>예약자 이름 : </td>
                        <td><%=bInfo.getB_uname() %></td>
                        <td>이메일 : </td>
                        <td><%=bInfo.getB_uid() %></td>
                    </tr>
                    <tr>
                        <td>연락처 : </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr class="topline bottomline title">
                        <th colspan="4">예약정보</th>
                    </tr>
                    <tr>
                        <td>예약일자 : </td>
                        <td><%=bInfo.getB_date() %></td>
                        <td>예약번호 : </td>
                        <td><%=bInfo.getB_idx() %></td>
                    </tr>
                    <tr>
                        <td>숙소 위치 : </td>
                        <td><%=hotel.getH_address1()%> <%=hotel.getH_address2()%> <%=hotel.getH_address3()%> <%=hotel.getH_address4()%></td>
                        <td>숙박기간 : </td>
                        <td><%=bInfo.getB_checkin() %> ~ <%=bInfo.getB_checkout() %></td>
                    </tr>

                    <tr>
                        <td>호텔이름 : </td>
                        <td><%=hotel.getH_title() %></td>
                        <td>예약 메세지:</td>
                        <td><%=bInfo.getB_msg() %></td>
                        
                    </tr>
               
                    <tr  class="topline bottomline title">
                        <th colspan="4">결제정보</th>
                    </tr>
                    <tr>
                        <td>총 이용 금액 : </td>
                        <td><%=bInfo.getB_price() %></td>
                        <td>결제상태 : </td>
                        <td><%if(bInfo.isB_deposit()){ %>결제 완료<%}else{ %>환불완료<%} %></td>
                    </tr>
                    <tr>
                        <td>최종 결제 금액 : </td>
                        <td><%=bInfo.getB_price() %></td>
                        <td>결제방법 : </td>
                        <td><%=bInfo.getB_card() %></td>
                    </tr>
                    <tr class="bottomline">
                        <td>결제일 : </td>
                        <td colspan="3"><%=bInfo.getB_date() %></td>
                    </tr>
                    <tr>
                        <td colspan="4"><input type="button" onclick="change(1)" value="목록"></td>
                    </tr>
                </table>
            </div>
            <div id="booking" style="<%=booking%>">
                <table class="table">
                    <caption>
                        <p>
                            <select name="select1">
                                <option value="예약번호">예약번호</option>
                                <option value="호텔이름">호텔이름</option>
                                <option value="예약자 이름">예약자 이름</option>
                            </select>
                            <input type="text">
                        </p>
                    </caption>
                    <tr>
                        <th>예약<br>번호</th>
                        <th>호텔 이름</th>
                        <th>예약일</th>
                        <th>숙박 인원</th>
                        <th>숙박 기간</th>
                        <th>진행단계</th>
                    </tr>
                     <%if(bList.size()==0){ %>
                    	<tr>
                    		<td colspan="6">예약이 없습니다.</td>
                    	</tr>
                    <%}else{ %>
                    	<%for(Booking b :bList){ %>
		                    <tr>
		                        <td><%=b.getB_idx()%></td>
		                        <td><a href="javascript:void(0);" onclick="BDetail(<%=b.getB_idx()%>)" ><%=b.getB_htitle() %></a></td>
		                        <td><%=b.getB_date() %></td>
		                        <td><%=b.getB_guests() %></td>
		                        <td><%=b.getB_checkin() %> ~ <br> <%=b.getB_checkout() %></td>
		                        <td><%if(b.isB_deposit()){ %>결제완료<%}else{ %>결제필요<%} %></td>
		                    </tr>
						<%} %>
                    <%} %>
                </table>
            </div>
        </div>
        </div>
			
		


</div>
        
        
<script>
	function BDetail(b_idx) {
		location.href="BookingDetail?b_idx="+b_idx;
	}
	function hPaging(action) {
		if(action == 'b'){ //빽
			var pn = <%=pageNum%>;
			if(pn > 1){
				pn= pn-1;
				location.href="HostMyHotel?pageNum="+pn;
			}
		}else if(action == 'f'){ //앞
			var pn = <%=pc%>;
			if(<%=pageNum%> < pn){
				
				location.href="HostMyHotel?pageNum="+<%=pageNum+1%>;
			}
		}else{
			var pn = action;
			location.href="HostMyHotel?pageNum="+pn;
		}
		
	}
</script>
	


</body>
</html>























