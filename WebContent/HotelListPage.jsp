<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dto.Hotel" %>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<p><a href="AdminPage.jsp">return admin page</a></p>
	<table border="1">
		<tr>
			<th>idx</th>
			<th>id</th>
			<th>Title</th>
			<th>Address1</th>
			<th>Address2</th>
			<th>Address3</th>
			<th>Price</th>
			<th>Type</th>
			<th>Guest</th>
		</tr>
		<%
			ArrayList<Hotel> list = (ArrayList<Hotel>)request.getAttribute("hlist");
			for (Hotel tmp : list){
				out.println("<tr>");
				out.println("<td>" + tmp.getH_idx() + "</td>");
				out.println("<td>" + tmp.getH_id() + "</td>");
				out.println("<td>" + tmp.getH_title() + "</td>");
				out.println("<td>" + tmp.getH_address1() + "</td>");
				out.println("<td>" + tmp.getH_address2() + "</td>");
				out.println("<td>" + tmp.getH_address3() + "</td>");
				out.println("<td>" + tmp.getH_price() + "</td>");
				out.println("<td>" + tmp.getH_type() + "</td>");
				out.println("<td>" + tmp.getH_guests() + "</td>");
				out.println("</tr>");
			}
		%>
	</table>
</body>
</html>