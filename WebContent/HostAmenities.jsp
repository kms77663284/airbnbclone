<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.dao.*" %>
    <%@ page import="java.util.List" %>
<%@ page import="com.dto.User" %>
    <%
    User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
    request.setCharacterEncoding("utf-8");
    FacilitiesDAO dao = FacilitiesDAO.getInstance();
    List<String> list = new ArrayList<String>();
    list = dao.SelectFacilities();
    
    %>
<!DOCTYPE html>
<html>
<head>
<jsp:useBean id="hotel" class="com.dto.Hotel"></jsp:useBean>
<jsp:setProperty property="*" name="hotel"/>
<%String myFacilities[] = hotel.getH_facilities().split(","); %>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     


<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">

                <p class="p_start">1단계 : 기본사항을 입력하세요.</p>
                <div class="progress">
                    <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="75"
                        aria-valuemin="0"
                        aria-valuemax="100"
                        style="width: 34%"></div>
                </div>
                <br>
                <div class="col-md-5 offset-1">
                    <p class="p_title">어떤 편의 시설을 제공하시나요?</p>
                    <p class="p_content">일반적으로 게스트가 기대하는 편의 시설 목록입니다. 숙소를 등록한 후 언제든 편의시설을 추가할수 있어요.</p>
                    <form action="HotelAddEdit" method="post" name="HostForm">
                        <div class="form-group">
                        <%int i=0;
                        for(String f_name : list){ %>
                            <div class="custom-control custom-checkbox">
                                <input name="h_facilities" value="<%=f_name %>" type="checkbox" class="custom-control-input" id="Check<%=i%>"
                                <%for(int j=0;j<myFacilities.length;j++){ 
                                	if(f_name.equals(myFacilities[j])){%>checked<%}else{ %><%}%>
                                <%}%>>
                                <label class="custom-control-label" for="Check<%=i%>"><span class="label-mt20"><%=f_name %></span></label>
                            </div>
                            <br>
                            <%i++;} %>
                        </div>
						<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                  		<input type="hidden" name="h_type" value="<%=hotel.getH_type()%>">
                     	<input type="hidden" name="h_type2" value="<%=hotel.getH_type2()%>">
                     	<input type="hidden" name="h_guests" value="<%=hotel.getH_guests()%>">
                     	<input type="hidden" name="h_address1" value="<%=hotel.getH_address1()%>">
                     	<input type="hidden" name="h_address2" value="<%=hotel.getH_address2()%>">
                     	<input type="hidden" name="h_address3" value="<%=hotel.getH_address3()%>">
                     	<input type="hidden" name="h_address4" value="<%=hotel.getH_address4()%>">
                     	<input type="hidden" name="h_zipcode" value="<%=hotel.getH_zipcode()%>">
                     	<input type="hidden" name="h_mapX" value="<%=hotel.getH_mapX()%>">
						<input type="hidden" name="h_mapY" value="<%=hotel.getH_mapY()%>">
                     	<input type="hidden" name="step" value="1">
                       
                    </form>
                </div>
     </div>
<jsp:include page="HostFooter.jsp" />
 </div>

</body>
</html>























