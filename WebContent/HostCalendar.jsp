<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
	request.setCharacterEncoding("utf-8");
	String name="민수";
%>
    
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     
<link href='<%=request.getContextPath() %>/css/core/main.min.css' rel='stylesheet' />
<link href='<%=request.getContextPath() %>/css/daygrid/main.min.css' rel='stylesheet' />
<link href='<%=request.getContextPath() %>/css/timegrid/main.min.css' rel='stylesheet' />
<script src='<%=request.getContextPath() %>/css/core/main.min.js'></script>
<script src='<%=request.getContextPath() %>/css/interaction/main.min.js'></script>
<script src='<%=request.getContextPath() %>/css/daygrid/main.min.js'></script>
<script src='<%=request.getContextPath() %>/css/timegrid/main.min.js'></script>
<jsp:useBean id="hotel" class="com.dto.Hotel"></jsp:useBean>
<jsp:setProperty property="*" name="hotel"/>

<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">
 <div class="container">
                <div>
                <p class="p_start">3단계 : 게스트를 맞이할 준비를 하세요</p>
                <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 90%"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="HotelAddEdit" method="post" name="HostForm">
                            <p class="p_title">달력을 업데이트 하세요.</p>
                            <p class="p_content">예약을 차단하거나 차단 해제할 날짜를 선택하세요.</p>
                          
                            <div id='calendar' name="calendar"></div>
                            <input type="hidden" id="cal" name="h_res_not_possible_date" value="<%=hotel.getH_res_not_possible_date()%>">


							<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                          	<input type="hidden" name="h_price" value="<%=hotel.getH_price()%>">
                          	<input type="hidden" name="h_rules" value="<%=hotel.getH_rules()%>">
                            <input type="hidden" name="step" value="3">
                           
                        </form>
                    </div>

            </div>
        </div>


       </div>
		</div>

<jsp:include page="HostFooter.jsp" />

</div>
        
        


<script>

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
	var selectDate= '';
    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
      selectable: true,
      unselectAuto:false,
      
      header: {
        left: 'prev,next',
        center: 'title',
        right: ''
        	
      },
      
     
      
      dateClick: function(info) {
    	    //alert('Clic	ked on: ' + info.dateStr);

    	    if(info.dayEl.style.backgroundColor == 'gray'){
    	    	info.dayEl.style.backgroundColor = 'white';//선택해제
    	    	var datesplit = selectDate.split(',');
    	    	var resultDate='';
    	    	for(var i in datesplit){
    	    		if(datesplit[i] == info.dateStr){
    	    			
    	    		}else{
    	    			if(datesplit[i]!=''){
    	    				resultDate +=datesplit[i]+',';
    	    			}
    	    		}
    	    	}
    	    	selectDate = resultDate;
    	    	
    	    	
    	    }else{
    	    	info.dayEl.style.backgroundColor = 'gray';//선택해제
    	    	selectDate += info.dateStr +',';
    	    }
    	    
    	    var t = document.getElementById("cal");
    	    t.value = selectDate;
    	   
    	    
    	  },
     // select: function(info) {
     //   alert('눌림 ' + info.startStr + ' ~ ' + info.endStr);
        
        
     // }
    });

    calendar.render();
  });
  
</script>
</body>
</html>



























        

       




