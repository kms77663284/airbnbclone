
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


    
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/host.css" >     


<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main">
	<div class="img-wrapper">
                <img class="img-responsive" src="images/hostmain.jpg">
                <div class="img-overlay">
                    <div class="form">
                            <h2>호스트가 되어</h2>
                            <h2>수입을 올려보세요.</h2>
                            <a href="HostMain.html" class="startbtn"><button class="btn_main" >호스트 시작하기</button></a>
                    </div>
                  
                </div>
            </div>
        
        
            
                <div style="margin-left: 4%;">
                    <div class="card border-secondary mb-3" style="max-width: 20rem; margin-top: 10px; margin-left: 5%; display:inline-flex ">
                        <div class="card-header">호스팅을 추천하는이유</div>  
                        <div class="card-body">
                            <p class="card-text">코리아비엔비는 호스트가 공유하는 숙소의 유형과 관계없이 게스트를 쉽고 안전하게 호스팅할 수있도록 해줍니다. 예약 가능일, 요금, 숙소 이용규칙 등등 방식들을 전적으로 호스트가 결정합니다</p>
                            </div>
                    </div>
                    <div class="card border-secondary mb-3" style="max-width: 20rem; margin-top: 10px; margin-left: 5%; display:inline-flex ">
                            <div class="card-header">호스트를 보호하는 코리아 비엔비의 노력</div>
                            <div class="card-body">
                                <p class="card-text">사고에 대비한 한화 100억원 재산 피해 보호 프로그램 및 별도의 미화 100억원 보험코리아 비엔비는 호스트, 호스트의 숙소와 재산 및 게스트를 보호할수있는 포괄적인 보호장치를 마련해두고 있습니다.</p>
                            </div>
                    </div>
                    <div class="card border-secondary mb-3" style="max-width: 20rem; margin-top: 10px; margin-left: 5%; display:inline-flex ">
                            <div class="card-header">게스트가 인증 되었습니다.</div>
                            <div class="card-body">
                            <p class="card-text">게스트가 예약하기전에 인증된 전화번호와 이메일 주소 등과 같은 정보를 제공하도록 하고 있습니다. 더많은 정보를 확인하고 싶다면 복인 인증 절차를 완료하고 다른호스트의 추천을 받은 게스트만 예약할수 있또록 필수요건을 설정하실수 있습니다</p>
                            </div>
                    </div>
                </div>
                </div>
                </div>
</body>
</html>

