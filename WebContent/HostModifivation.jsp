<%@page import="com.dto.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@ page import="com.dto.*" %>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
    	Hotel hotel = new Hotel();
		if(request.getAttribute("hotelinfo")!=null){
			hotel = (Hotel)request.getAttribute("hotelinfo");
		}

		
		
    %>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     


<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main">

            <div class="row">
                <div class="col-md-5 offset-1 div-margin-top">
                        <p class="p_stitle"><%=user.getU_last_name() %>님 숙소 등록을 계속 진행해 볼까요?</p>
                        <p class="p_content">이제 숙소를 등록할 수 있도록 몇가지 세부정보를 추가하도록 하겠습니다.</p>
                        
                            <p class="p_content">숙소유형,종류 편의시설 등</p>
                            <form action="HostRoom.jsp" method="post">
                            	<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                            	<input type="hidden" name="h_type" value="<%=hotel.getH_type()%>">
                            	<input type="hidden" name="h_type2" value="<%=hotel.getH_type2()%>">
                            	<input type="hidden" name="h_guests" value="<%=hotel.getH_guests()%>">
                            	<input type="hidden" name="h_address1" value="<%=hotel.getH_address1()%>">
                            	<input type="hidden" name="h_address2" value="<%=hotel.getH_address2()%>">
                            	<input type="hidden" name="h_address3" value="<%=hotel.getH_address3()%>">
                            	<input type="hidden" name="h_address4" value="<%=hotel.getH_address4()%>">
                            	<input type="hidden" name="h_zipcode" value="<%=hotel.getH_zipcode()%>">
                            	<input type="hidden" name="h_facilities" value="<%=hotel.getH_facilities()%>">

                            	<input class="btn btn-secondary" role="button" type="submit" value="<%if(hotel.getH_type()==null){%>계속 <%}else{%>변경<%}%>"><br>
                            </form>
                            <p class="p_content">2단계</p>
                            <p>상세정보를 제공해주세요.</p>
                            <p class="p_content">사진,간단한 설명,제목</p>
                            <form action="HostPotos.jsp" method="post">
                            	<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                            	<input type="hidden" name="h_title" value="<%=hotel.getH_title()%>">
                            	<input type="hidden" name="h_introduce" value="<%=hotel.getH_introduce()%>">
                            	<input type="hidden" name="h_other" value="<%=hotel.getH_other()%>">
                            	
                            	<%if(hotel.getH_facilities()==null){}else{ %><input class="btn btn-secondary" role="button" type="submit" value="<%if(hotel.getH_title()==null){%>계속 <%}else{%>변경<%}%>"><%} %><br>
                            </form>
                        <br>
                        
                            <p class="p_content">3단계</p>
                            <p class="p_content">게스트를 맞이할 준비를 하세요</p>
                            <p class="p_content">예약설정,달력,요금</p>
                            <form action="HostRules.jsp" method="post">
                            	<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                            	<input type="hidden" name="h_price" value="<%=hotel.getH_price()%>">
                            	<input type="hidden" name="h_res_not_possible_date" value="<%=hotel.getH_res_not_possible_date()%>">
                            	<input type="hidden" name="h_rules" value="<%=hotel.getH_rules()%>">
                            	<%if(hotel.getH_title()==null){}else{ %><input class="btn btn-secondary" role="button" type="submit" value="<%if(hotel.getH_rules()==null){%>계속 <%}else{%>변경<%}%>"><%} %><br>
                            </form>
                        <form action="HostMyHotel" method="post">
                        	<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                        	<input class="btn btn-secondary hoteladd" role="button" type="submit" value="숙소등록">
                        </form>
                </div>
                <div class="col-lg-5">
                        <img src="<%=request.getContextPath() %>/images/Modification.png" class="modiimg" >
                </div>
            </div>
        </div>
		


</div>
        
        



</body>
</html>























