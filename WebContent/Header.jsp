<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.dto.User" %>
<%User user = (User)session.getAttribute("user");
if (user == null){response.sendRedirect("Login.jsp");}%>
<%String search=request.getParameter("search"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap-datepicker.css">
<script src="https://code.jquery.com/jquery-latest.js"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap-datepicker.js"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap-datepicker.ko.min.js"></script>
<link  href="<%=request.getContextPath() %>/css/loginbar.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/header.css" rel="stylesheet" >
</head>
<body>
	<header>
		<div id="headerdiv">
			<form method="get" action="Search" id="headerForm">
				<a href="<%=request.getContextPath() %>/MainPage.jsp"><img src="<%=request.getContextPath() %>/images/logo.png" id="logo_image"></a>
				<input type="text" name="search" placeholder="목적지" id="search_input" value="<%=search==null?"":search%>">
				<input type="button" id="f_button" value="필터" onclick="openfillter()">
	            <input type="hidden" id="h_page" name="page" value="<%=(request.getParameter("page")==null||request.getParameter("page").equals(""))?1:Integer.parseInt(request.getParameter("page"))%>">
			<%if(user == null){ //로그인 안된상태 %>
				<nav class="menubar">
					<ul>
						<li><a href="Join.jsp">회원가입</a></li>
						<li><a href="Login.jsp">로그인</a></li>
					</ul>
					<div class="menu_container"><i class="fa fa-bars"></i></div> 
				</nav>
			<%}else{%>
				<div class="menubar">
					<ul>
						<li><a href="HostMyHotel" id="current">호스트</a>
							<ul>
								<li><a href="HotelMyList">숙소추가</a></li>
								<li><a href="HostMyHotel">내 숙소</a></li>
								<li><a href="HostMyBooking">예약 내역</a></li>
							</ul>
						</li>
						<li><a href="BookmarkInfo">저장목록</a></li>
						<li><a href="#">도움말</a>
							<ul>
								<li><a href="#서비스소개">서비스 소개</a></li>
								<li><a href="#이용약관">이용약관</a></li>
								<li><a href="">공지사항</a></li>
								<li><a href="">문의</a></li>
							</ul>
						</li>
						<li><a href="MyPage.jsp"><img src="<%=request.getContextPath() %>/images/avatar04.png" id="img" alt="" style=""></a>
							<ul>
								<li><a href="MyPage.jsp">계정</a></li>
								<li><a href="mypage_res?page=1">예약 내역</a></li>
								<li><a href="Logout">로그아웃</a></li>
							</ul>
						</li>
					</ul>
				</div>
			<%} %>
				<div id="fillter">
					<div class="rows">
						<label for="person">기간</label>
						<div class="cols">
							<input name="dateStart" value="체크인" type="text" class="form-control input-group date">
						</div>
						<span>&nbsp;~&nbsp;&nbsp;</span>
						<div class="cols">
							<input name="dateEnd" value="체크아웃" type="text" class="form-control input-group date">
						</div>
					</div>
					<div class="rows">
						<label for="person">인원</label>
						<div class="cols">
							<select name="guests" class="form-control" id="person">
								<option value="인원">인원</option>
								<option value="1">1명</option>
								<option value="2">2명</option>
								<option value="3">3명</option>
								<option value="4">4명</option>
								<option value="5">5명</option>
								<option value="6">6명</option>
								<option value="7">7명</option>
								<option value="8">8명</option>
								<option value="9">9명</option>
								<option value="10">10명</option>
								<option value="11">11명</option>
								<option value="12">12명</option>
								<option value="13">13명</option>
								<option value="14">14명</option>
								<option value="15">15명+</option>
							</select>
						</div>
						<label for="exampleSelect1">유형</label>
						<div class="cols">
							<select name="type1" class="form-control" id="exampleSelect1">
								<option value="유형">유형</option>
								<option value="개인실">개인실</option>
								<option value="다인실">다인실</option>
								<option value="집전체">집전체</option>
							</select>
						</div>
					</div>
					<div class="rows">
						<label>가격</label>
						<div class="cols" style="width: 70%;">
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">$</span>
								</div>
								<input name="price" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</header>
</body>
	<script>
		function openfillter() {
			var ft = document.getElementById('fillter');
			if (ft.style.display == 'none') {
				ft.style.display = 'block';
			} else {
				ft.style.display = 'none';
			}
		}
		$(function () {
			$('.input-group.date').datepicker({
				calendarWeeks: false, todayHighlight: true, autoclose: true, //사용자가 날짜를 클릭하면 자동 캘린더가 닫히는 옵션
				datesDisabled: [], //선택 불가능한 일 설정 하는 배열 위에 있는 format 과 형식이 같아야함.
				format: "yyyy-mm-dd",
				multidate: false, //여러 날짜 선택할 수 있게 하는 옵션 기본값 :false
				multidateSeparator: ",", //여러 날짜를 선택했을 때 사이에 나타나는 글짜 2019-05-01,2019-06-01
				language: "ko"
			});
			
		});
		$("#search_input").keypress(function(key){
			if (key.keyCode == 13){
				$("#headerForm").submit();
			}
		});
	</script>
</html>