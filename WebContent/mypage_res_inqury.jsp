<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
%>

<%@ page import="com.dto.Booking" %>
<%@ page import="com.dto.User" %>
<%@ page import="com.dto.Hotel" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Insert title here</title>
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/mypage.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/mypage_inqury.css">
 	<style>
        #wrap {
            width: 100%; 
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
        }
    </style>
<%
	Booking data = (Booking)request.getAttribute("booking");
	data = data==null?new Booking():data;
	Hotel hotel = (Hotel)request.getAttribute("hotel");
	hotel = hotel==null?new Hotel():hotel;
	User host = (User)request.getAttribute("host");
	host = host==null?new User():host;
%>
</head>
<body>
<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main">
  <div class="container">
            <div class="container_box">
                <span class="title_box">
                    <a href="mypage.html">계정 관리</a>
                    <span>&gt;</span>
                    <a href="mypage_res.html">예약 내역</a>
                    <span>&gt;</span>
                    <a href="#" class="privacy_a">예약 조회</a>
                </span>
                <h2>예약 상세 내역</h2>

                <div class="content_box1">
                    <h3>예약자 정보</h3>
                    <table>
                        <tr>
                            <th>예약자명 :</th>
                            <td><%=user.getU_first_name() %> <%=user.getU_last_name() %></td>
                            <th>이메일 :</th>
                            <td><%=user.getU_id() %></td>
                        </tr>
                        <tr colspan="2">
                            <th>전화번호 :
                            </th>
                            <td><%=user.getU_phone() %></td>
                        </tr>
                    </table>

                    <h3>예약 정보</h3>
                    <table>
                        <tr>
                            <th>예약 일자 :</th>
                            <td><%=data.getB_date() %></td>
                            <th>예약 번호 :</th>
                            <td><%=data.getB_idx() %></td>
                        </tr>
                        <tr>
                            <th>숙박 지역 :</th>
                            <td><%=hotel.getH_address1() %> <%=hotel.getH_address2() %></td>
                            <th>숙박 기간 :</th>
                            <td><%=data.getB_checkin() %> ~ <%=data.getB_checkout() %></td>
                        </tr>
                        <tr>
                            <th>호텔 이름 :</th>
                            <td><%=hotel.getH_title() %></td>
                            <th>예약 메세지 :</th>
                            <td><%=data.getB_msg() %></td>
                        </tr>
                        <tr>
                            <th>호스트 이름 :</th>
                            <td><%=host.getU_first_name() %> <%=host.getU_last_name() %></td>
                            <th>호스트 번호 :</th>
                            <td><%=host.getU_phone() %></td>
                        </tr>
                        <tr>
                            <th>상세주소 :
                            </th>
                            <td colspan="2"><%=data.getB_address() %></td>
                        </tr>
                    </table>
                    <div id="map" style="width:100%;height:200px;"></div>
                    <h3>결제 정보</h3>
                    <table>
                        <tr>
                            <th>총 이용 금액 :</th>
                            <td><%=data.getB_price() %></td>
                            <th>결제 상태 :</th>
                            <td><%=data.isB_deposit()?"결제완료":"결제진행중" %></td>
                        </tr>
                        <tr>
                            <th>최종 결제 금액 :
                            </th>
                            <td><%=data.getB_price() %></td>
                            <th>결제 방법 :</th>
                            <td>신용카드(일시불)</td>
                        </tr>
                    </table>
                    <div class="buttons">
                        <button onclick="mylist()">
                            	목록
                        </button>
                        <button>
                            	환불신청
                        </button>
                    </div>
                </div>
            </div>

		</div>
        </div>
</div>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=1745d308df291261c0f18ff1268d942e&libraries=services,clusterer"></script>
            <script>
	        	$(function(){
	        		var mapContainer = document.getElementById('map'),
	        	    mapOption = {
	        	        center: new kakao.maps.LatLng(<%=hotel.getH_mapY()%>,<%=hotel.getH_mapX()%>),
	        	        level: 5
	        	    };    
	        		var map = new kakao.maps.Map(mapContainer, mapOption); 
	    				var imageSrc = 'images/marker_1.png',
	    		    		imageSize = new kakao.maps.Size(34, 40),
	    		    		imageOption = {offset: new kakao.maps.Point(27, 69)};
	    		
	    				var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
	    				    markerPosition = new kakao.maps.LatLng(<%=hotel.getH_mapY()%>,<%=hotel.getH_mapX()%>);
	    		
	    				var marker = new kakao.maps.Marker({
	    				  position: markerPosition,
	    				  image: markerImage
	    				});
	
	    				marker.setMap(map);  
	        	});
                function mylist() {
                    location.href = "mypage_res?page=1";
                }
            </script>
</body>
</html>