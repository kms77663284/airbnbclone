<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.Hotel" %>
<%@ page import="com.dao.HotelDAO" %>
<%@ page import="com.dto.User" %>
<%
	User user =null;
	
	if(session.getAttribute("user")!=null){
		user= (User)session.getAttribute("user");
	}
	if (user == null){response.sendRedirect("Login.jsp");}
%>
<%
request.setCharacterEncoding("utf-8");
String h_idx = request.getParameter("h_idx");
HotelDAO dao = HotelDAO.getInstance();
Hotel hotel = dao.selectHotelInfo(h_idx);
String ppl = request.getParameter("ppl");
String checkin =request.getParameter("b_checkin");
String checkout =request.getParameter("b_checkout");
String startYYYY = checkin.substring(0,4);
String startMM = checkin.substring(5,7);
String startDD = checkin.substring(8,10);
String endYYYY=checkout.substring(0,4);
String endMM=checkout.substring(5,7);
String endDD=checkout.substring(8,10);
String b_msg = request.getParameter("b_msg");


long date=Integer.parseInt(request.getParameter("date"));
int price = (int)(10000+hotel.getH_price() * date * Integer.parseInt(ppl) +(hotel.getH_price() * date)*0.10+(hotel.getH_price() * date)*0.01);

%>
<!DOCTYPE html>
<html>


 <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link
            href="https://fonts.googleapis.com/css?family=Bad+Script&display=swap"
            rel="stylesheet">
        <link
            href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800&display=swap&subset=korean"
            rel="stylesheet">
        <title>KoreaBnb</title>
        <link rel="stylesheet" href="css/reserve3.css">
    </head>
    <body>
        <header>
            <div>
                <img src="images/logo.png" id="logo_image">
                <nav id="topmenu">
                    <span class="bold1">
                        1. 숙소 이용규칙 확인
                        <span id="n">
                            >
                        </span>
                    </span>
                    <span class="bold1">
                        2. 게스트 정보 입력
                        <span id="n">
                            >
                        </span>
                    </span>
                    <span class="bold1">
                        3. 확인 및 결제</span>
                </nav>
            </div>
        </header>
        <div id="container">
            <div id="rule-box">
                <div id="title">
                    <h2>확인 및 결제</h2>
                </div><br>
                <form action="reserve_forth" method="post">
                <div id="pay">
                    
                        결제 수단<br><input type="text" name="b_card" placeholder="신용카드 또는 체크카드">

                        <br>이름<br><input type="text" name="first_name" placeholder="이름">
                        <br>성<br><input type="text" name="last_name" placeholder="성">

                        <br>카드 정보<br><input type="text" id="card_info" name="b_card_info" placeholder="카드번호">
                        <br><input type="text" id="card_info1" name="card_info1" placeholder="만료일">
                        <input type="text" id="card_info2" name="card_info2" placeholder="CVV">

                        <br>청구지 정보<br><input type="text" id="post" name="post" placeholder="우편번호">
                        <br>국가/지역<br><input type="text" id="country" name="country" placeholder="한국">
                    
                </div>
                <div id="button">
                	<input type="hidden" value="<%=price%>" name="price">
                	<input type="hidden" value="<%=checkin %>" name="b_checkin">
               		<input type="hidden" value="<%=checkout %>" name="b_checkout">
                   	<input type="hidden" value="<%=hotel.getH_idx() %>" name="h_idx">
                   	<input type="hidden" value="<%=date %>" name="date">
                   	<input type="hidden" value="<%=ppl %>" name="ppl">
                   	<input type="hidden" value="<%=b_msg %>" name="b_msg">
                    <input type="submit" value="확인 및 결제" >
                </div>
                </form>
            </div>
                     <div id="margin-box"></div>
           <div id="pay-box">
                <div id="popup">
                        <div id="popuptitle">
                            <h4><%=hotel.getH_title() %></h4>
                            <h5><%=hotel.getH_address1() %>의 <%=hotel.getH_type2() %>의 <%=hotel.getH_type() %><br><br></h5>
                            <img src="images/icon4_1.jpg" id="popup_img">
                            게스트 <%=ppl %>명<br><br>
                            <img src="images/icon4_2.jpg" id="popup_img">
                            <%=startYYYY %>년 <%=startMM %>월 <%=startDD %>일 -> <%=endYYYY %>년 <%=endMM %>월 <%=endDD %>일
                        </div>
                        <div class="content3">
                            <div class="content3_1 content3_2 content3_3">
                                ₩<%=hotel.getH_price() %> x <%=date%>박 x <%=ppl %>명
                                <span class="money">₩<%=hotel.getH_price() * date * Integer.parseInt(ppl)%></span></div>
                            <div class="content3_1 content3_3">
                                청소비
                                <span class="money">₩10,000</span></div>
                            <div class="content3_1 content3_3">
                                서비스 수수료
                                <span class="money">₩<%=Math.floor((hotel.getH_price() * date)*0.10)%></span></div>
                            <div class="content3_1 content3_3">
                                숙박세와 수수료<span class="money">₩<%=Math.floor((hotel.getH_price() * date)*0.01)%></span></div>
                        </div>
                        <div class="content3">
                            <div class="content3_1 content3_4">
                                총 합계 (KRW)
                                <span class="money">₩<%=Math.floor(10000+hotel.getH_price() * date * Integer.parseInt(ppl) +(hotel.getH_price() * date)*0.10+(hotel.getH_price() * date)*0.01) %></span></div>
                        </div>
                  
                </div>
            </div>
        </div>
    </body>

</html>