CREATE DATABASE koreabnb DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
DROP DATABASE koreabnb
USE koreabnb

create table userprivacy(
	u_id 	VARCHAR(100) not NULL primary key,
	u_pwd varchar(500) not null,
	u_last_name varchar(45) not null,
	u_first_name varchar(45) not null,
	u_residence VARCHAR(150),
	u_company VARCHAR(150),
	u_introduce text,
	u_activation bool default true not null,
	u_potoaddress text,
	admin BOOL default false not NULL,
	u_date	datetime default CURRENT_TIMESTAMP(),
	u_login DATETIME default CURRENT_TIMESTAMP()
)
SELECT * FROM userprivacy WHERE u_id = 'korea@gmail.com' AND u_pwd = PASSWORD('1234')

create table hotelinfo(
	h_idx	int	primary key auto_increment not null,
	h_id VARCHAR(100) not null,
	h_introduce text,
	h_other text,
	h_price int,
	h_title text,
	h_star float default 0,
	h_address1 VARCHAR(100),
	h_address2 VARCHAR(100),
	h_address3 VARCHAR(100),
	h_address4 VARCHAR(100),
	h_zipcode VARCHAR(30),
	h_type varchar(50),
	h_guests int,
	h_grant bool default false not null,
	h_facilities text,
	h_type2 text,
	h_rules text,
	h_res_not_possible_date text,
	h_mapX double,
	h_mapY double,
	h_date	datetime default CURRENT_TIMESTAMP(),
	foreign key (h_id) references userprivacy(u_id)
)



create table booking(
	b_idx int primary key auto_increment,
	h_idx int not null,
	u_id VARCHAR(100) NOT NULL,
	h_id VARCHAR(100) NOT NULL,
	b_checkin VARCHAR(100) not null,
	b_checkout VARCHAR(100) not null,
	b_date	datetime default CURRENT_TIMESTAMP(),
	b_price	int not null,
	b_deposit bool default false,
	b_guests INT not null,
	b_msg text,
	b_card VARCHAR(100),
	b_card_info VARCHAR(150),
	
	foreign key(u_id) references userprivacy(u_id),
	foreign key(h_idx) references hotelinfo(h_idx)
)

create table review(
	r_idx int primary key auto_increment,
	u_id VARCHAR(100) not null,
	h_idx int not null,
	r_content text not null,
	r_date datetime default CURRENT_TIMESTAMP(),
	r_star float not null,
	foreign key(u_id) references userprivacy(u_id),
	foreign key(h_idx) references hotelinfo(h_idx)
)

create table adminlist(
	a_id VARCHAR(100) primary key,
	a_pass varchar(500) not null
)

create table bookmark(
	f_idx	int primary key auto_increment,
	h_idx	int not null,
	u_id	VARCHAR(100) not null,
	foreign key(h_idx) references hotelinfo(h_idx),
	foreign key(u_id) references userprivacy(u_id)
)

create table question(
   q_idx   int primary key auto_increment,
   u_id   VARCHAR(100) not null,
   q_title   varchar(150) not null,
   q_content TEXT,
   q_answer TEXT,
   q_date datetime default CURRENT_TIMESTAMP(),
   foreign key(u_id) references userprivacy(u_id)
)

create table notice(
	n_idx	int primary key auto_increment,
	a_id	VARCHAR(100) not null,
	n_title varchar(150) not null,
	n_content text,
	n_vcount int default 0,
	n_date datetime default CURRENT_TIMESTAMP(),
	foreign key(a_id) references adminlist(a_id)
)

create table facilities(
	f_name varchar(200)
)

create table rules(
	rules_name varchar(200)
)

create table hoteltype1(
	h_type varchar(45) not null unique
)

create table hoteltype2(
	h_type varchar(45) not null unique
)


insert into hoteltype2 values('아파트');
insert into hoteltype2 values('펜션');
insert into hoteltype2 values('주텍');

insert into hoteltype1 values('집 전체');
insert into hoteltype1 values('개인실');


INSERT INTO facilities (f_name) VALUE ('필수품목');
INSERT INTO facilities (f_name) VALUE ('헤어드라이기');
INSERT INTO facilities (f_name) VALUE ('난방');
INSERT INTO facilities (f_name) VALUE ('무선인터넷');
INSERT INTO facilities (f_name) VALUE ('옷장/서랍');

INSERT INTO rules(rules_name) VALUE ('파티')

/* 한글 될때 테이블마다 해줘야함*/
ALTER TABLE facilities CONVERT TO character SET UTF8;  
ALTER TABLE userprivacy CONVERT TO character SET UTF8;  
ALTER TABLE bookmark CONVERT TO character SET UTF8; 
ALTER TABLE hotelinfo CONVERT TO character SET UTF8;
  
SELECT * FROM userprivacy
SELECT * FROM hotelinfo
SELECT * FROM facilities
SELECT * FROM bookmark
SELECT * FROM booking
SELECT * FROM Review


